import {AfterViewInit, Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AccountsService} from '../../services/accounts.service';
import {Account} from '../../shared/models/account.model';
import {TransfersHistoryService} from '../../services/transfers-history.service';
import {User} from '../../shared/models/user.model';
import {UsersService} from '../../services/users.service';

@Component({
  selector: 'app-account-details',
  templateUrl: './account-details.component.html',
  styleUrls: ['./account-details.component.scss']
})
export class AccountDetailsComponent implements AfterViewInit, OnInit {
  user: User;
  account: Account;

  constructor(
    private route: ActivatedRoute,
    private accountsService: AccountsService,
    private transfersHistoryService: TransfersHistoryService,
    private usersService: UsersService) {
  }

  ngOnInit(): void {
    this.user = this.usersService.user;
    this.route.params.subscribe(params => {
      this.accountsService.getAccountDetails(params['id']).subscribe(data => {
        console.log(data);
        this.account = data;
      })
    });
  }

  ngAfterViewInit(): void {
  }
}
