import {Component, Inject} from '@angular/core';
import {Transfer} from '../../../shared/models/transfer.model';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {pdfGenerator} from '../../../shared/generators/pdf.generator';
import {TransferService} from '../../../services/transfer.service';

@Component({
  selector: 'app-transfer-dialog',
  templateUrl: './transfer-dialog.component.html',
  styleUrls: ['./transfer-dialog.component.scss'],
  providers: [
    { provide: 'Window', useValue: window }
  ]
})
export class TransferDialogComponent {

  constructor(public dialogRef: MatDialogRef<TransferDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: Transfer,
              @Inject('Window') private window: Window,
              private transfersService: TransferService) {
    console.log(data);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  generatePdf() {
    console.log(this.data);
    pdfGenerator([this.data]);
  }
}
