import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {Transfer, TRANSFER_STATUSES_NAMES} from '../../../shared/models/transfer.model';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import {User} from '../../../shared/models/user.model';
import {TransfersHistoryService} from '../../../services/transfers-history.service';
import {ActivatedRoute} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {pdfGenerator} from '../../../shared/generators/pdf.generator';
import {TransferDialogComponent} from '../transfer-dialog/transfer-dialog.component';

@Component({
  selector: 'app-transfers-table',
  templateUrl: './transfers-table.component.html',
  styleUrls: ['./transfers-table.component.scss']
})
export class TransfersTableComponent implements OnInit {

  @Input('direct')
  direct: string;

  transfers$: Observable<Transfer[]>;

  anyTransactionChecked = false;

  isSpinnerVisible: boolean;
  isTableVisible: boolean;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  dataSource: MatTableDataSource<User>;
  displayedColumns = ['select', 'date', 'title', 'amount', 'status'];
  selection = new SelectionModel<Transfer>(true, []);

  constructor(public dialog: MatDialog,
              private route: ActivatedRoute,
              private transfersHistoryService: TransfersHistoryService) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.transfers$ = this.transfersHistoryService.getAccountsTransfersHistory(params['id'], this.direct);
      this.transfers$.subscribe(x => console.log(x));
      this._initialize();
    });

    this.selection.onChange.subscribe(() => {
      this.anyTransactionChecked = this.selection.selected.length > 0;
    });
  }

  generatePdf() {
    console.log(this.selection.selected);
    pdfGenerator(this.selection.selected);
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  private _initialize() {
    this.dataSource = null;
    this.isSpinnerVisible = true;
    this.transfers$.subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.isTableVisible = true;
      this.isSpinnerVisible = false;
    });
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }

  openDialog(row: Transfer) {
    this.dialog.open(TransferDialogComponent, {
      width: '100%',
      data: row
    });
  }

  getStatus(statusCode: number) {
    return TRANSFER_STATUSES_NAMES.get(statusCode);
  }
}
