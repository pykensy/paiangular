import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AccountsComponent} from './accounts/accounts.component';
import {AccountsRoutingModule} from './accounts.routing';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SharedModule} from '../shared/shared.module';
import {ListComponent} from './list/list.component';
import {AccountComponent} from './list/account/account.component';
import {AccountsService} from '../services/accounts.service';
import {AccountDetailsComponent} from './account-details/account-details.component';
import {TransfersHistoryService} from '../services/transfers-history.service';
import {TransferDialogComponent} from './account-details/transfer-dialog/transfer-dialog.component';
import {TransfersTableComponent} from './account-details/transfers-table/transfers-table.component';
import {MatIconModule} from '@angular/material';

@NgModule({
  imports: [
    BrowserModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    AccountsRoutingModule,
    MatIconModule
  ],
  declarations: [AccountsComponent, ListComponent, AccountComponent, AccountDetailsComponent, TransferDialogComponent, TransfersTableComponent],
  providers: [AccountsService, TransfersHistoryService],
  entryComponents: [TransferDialogComponent]
})
export class AccountsModule { }
