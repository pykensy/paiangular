import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {AccountsComponent} from './accounts/accounts.component';
import {AuthGuard} from '../shared/guards/auth-guard.service';
import {AccountDetailsComponent} from './account-details/account-details.component';

const routes: Routes = [
  {
    path: 'accounts',
    component: AccountsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'accounts/:id',
    component: AccountDetailsComponent,
    canActivate: [AuthGuard],
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AccountsRoutingModule {
}
