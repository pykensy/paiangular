import {Component, OnInit} from '@angular/core';
import {UsersService} from '../../services/users.service';
import {User} from '../../shared/models/user.model';

@Component({
  selector: 'app-accounts',
  templateUrl: './accounts.component.html',
  styleUrls: ['./accounts.component.scss']
})
export class AccountsComponent implements OnInit {

  user: User;

  constructor(private usersService: UsersService) {
    this.user = usersService.user;
  }

  ngOnInit() {
  }

}
