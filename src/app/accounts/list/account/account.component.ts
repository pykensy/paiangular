import {Component, Input, OnInit} from '@angular/core';
import {Account} from '../../../shared/models/account.model';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {

  @Input('account')
  account: Account;

  constructor() { }

  ngOnInit() {
  }

}
