import {Component, Input, OnInit} from '@angular/core';
import {AccountsService} from '../../services/accounts.service';
import {Observable} from 'rxjs/Observable';
import {Account} from '../../shared/models/account.model';
import {User} from '../../shared/models/user.model';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  accounts$: Observable<Account[]>;

  @Input('user')
  user: User;

  constructor(private accountsService: AccountsService) {
  }


  ngOnInit() {
    this.accounts$ = this.accountsService.getUserAccounts(this.user.id);
  }

}
