export const LOGO_PATH = 'assets/images/logo.png';


export const API_URL = 'http://localhost:8080/bank/api/';
export const AUTH_API_URL = 'http://localhost:8080/bank/api/auth/token';

export const ADMIN_ROLE = 'ADMIN';
export const EMPLOYEE_ROLE = 'EMPLOYEE';
export const USER_ROLE = 'USER';
