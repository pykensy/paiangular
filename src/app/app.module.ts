import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {SharedModule} from './shared/shared.module';
import {CommonModule} from '@angular/common';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {RouterModule} from '@angular/router';
import {LayoutModule} from './layout/layout.module';
import {HomeModule} from './home/home.module';
import {HttpClientModule} from '@angular/common/http';
import {ReactiveFormsModule} from '@angular/forms';
import {AppRoutingModule} from './app.routing';
import {LoginModule} from './login/login.module';
import {AccountsModule} from './accounts/accounts.module';
import {TransfersModule} from './transfers/transfers.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    CommonModule,
    ReactiveFormsModule,
    SharedModule,
    AppRoutingModule,
    RouterModule,
    LayoutModule,
    HomeModule,
    HttpClientModule,
    LoginModule,
    AccountsModule,
    TransfersModule
  ],
  exports: [
    BrowserModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    CommonModule,
    SharedModule,
    LayoutModule,
    HomeModule,
    AppRoutingModule,
    LoginModule,
    AccountsModule,
    TransfersModule
  ],
  providers: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
