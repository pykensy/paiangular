import {Component} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {User} from '../../shared/models/user.model';
import {MatSnackBar} from '@angular/material';
import {isRequestSuccess} from '../../shared/http/request-status';
import {AuthenticationService} from '../../services/authentication.service';
import {ActivatedRoute, Router} from '@angular/router';
import {UsersService} from '../../services/users.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  LOGIN_SUCCESS_MSG = 'You have been logged in';
  LOGIN_ERROR_MSG = 'You have not been logged in';

  form: FormGroup;
  user = new User();

  isFormVisible = true;
  isSpinnerVisible = false;

  private redirectUrlAdminSuccess = 'admin';
  private redirectUrlUserSuccess = 'accounts';

  constructor(public snackBar: MatSnackBar,
              private router: Router,
              private activatedRoute: ActivatedRoute,
              private authenticationService: AuthenticationService,
              private usersService: UsersService,
              private fb: FormBuilder) {
    this.redirectIfLogged();
    this._initForm();
  }

  private _initForm() {
    this.form = this.fb.group({
      username: ['111111', Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(20)])],
      password: ['12345', Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(20)])]
    });
  }

  login() {
    this.user = this.form.value;
    this.authenticationService.login(this.user).subscribe(data => {
      console.log(data);
      const result = isRequestSuccess(data);
      const msg = result ? this.LOGIN_SUCCESS_MSG : this.LOGIN_ERROR_MSG;
      this.openSnackBar(msg);
      if (result) {
        this.usersService.login(data.body);
        this.isFormVisible = false;
        this.isSpinnerVisible = true;
        setTimeout(() => this.navigateAfterSuccess(), 3000);
      }
    });
  }

  openSnackBar(message: string) {
    this.snackBar.open(message, null, {
      duration: 5000,
    });
  }

  private navigateAfterSuccess() {
    this.redirectIfLogged();
  }

  private redirectIfLogged() {
    if (this.usersService.isLogged()) {
      if (this.usersService.isAdmin || this.usersService.isEmployee) {
        this.router.navigate([this.redirectUrlAdminSuccess]);
      } else if (this.usersService.isUser) {
        this.router.navigate([this.redirectUrlUserSuccess]);
      }
    }
  }
}
