import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {AUTH_API_URL} from '../app.consts';
import {Observable} from 'rxjs/Observable';
import {User} from '../shared/models/user.model';

@Injectable()
export class AuthenticationService {

  constructor(private http: HttpClient) {

  }

  login(user: User): Observable<any> {
    const body = `username=${encodeURIComponent(user.username)}&password=${encodeURIComponent(user.password)}`;
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type', 'application/x-www-form-urlencoded');
      // .append('Authorization', 'Basic ' + btoa(TOKEN_AUTH_USERNAME + ':' + TOKEN_AUTH_PASSWORD));
    return this.http.post<any>(AUTH_API_URL, body, {headers: headers, observe: 'response'});
  }
}
