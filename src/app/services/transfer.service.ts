import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {TransferType} from '../shared/models/transfer-type.model';
import {API_URL} from '../app.consts';
import {TransferRequest} from '../shared/models/transfer-request.model';

@Injectable()
export class TransferService {

  constructor(private http: HttpClient) { }

  getTransferTypes(): Observable<TransferType[]> {
    return this.http.get<TransferType[]>(API_URL + 'transfer/types');
  }

  generateToken(transferRequest: TransferRequest): Observable<any> {
    return this.http.post<any>(API_URL + 'transfers/token', transferRequest)
  }

  tryExecuteTransfer(transferRequest: TransferRequest): Observable<any> {
    return this.http.post<any>(API_URL + 'transfers', transferRequest);
  }
}
