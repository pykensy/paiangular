import {inject, TestBed} from '@angular/core/testing';

import {TransfersHistoryService} from './transfers-history.service';

describe('TransfersHistoryService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TransfersHistoryService]
    });
  });

  it('should be created', inject([TransfersHistoryService], (service: TransfersHistoryService) => {
    expect(service).toBeTruthy();
  }));
});
