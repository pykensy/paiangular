import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {API_URL} from '../app.consts';
import {Transfer} from '../shared/models/transfer.model';

@Injectable()
export class TransfersHistoryService {

  constructor(private http: HttpClient) { }

  getAccountsTransfersHistory(id: string, direct: string): Observable<Transfer[]> {
    return this.http.get<Transfer[]>(API_URL + 'transfers/account/history/' + direct + '/' + id);
  }

}
