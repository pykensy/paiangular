import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {User} from '../shared/models/user.model';
import {ADMIN_ROLE, API_URL, EMPLOYEE_ROLE} from '../app.consts';
import {JwtHelper} from 'angular2-jwt';
import {TOKEN_NAME} from './auth.constant';

@Injectable()
export class UsersService {
  jwtHelper: JwtHelper = new JwtHelper();
  accessToken: string;
  isAdmin: boolean;
  isEmployee: boolean;
  user: User;

  constructor(private http: HttpClient) {
    const token = localStorage.getItem(TOKEN_NAME);
    if (token != null) {
      this._initializeRoles(token);
    }
  }

  getAll(): Observable<User[]> {
    return this.http.get<User[]>(API_URL + '/users');
  }

  getUser(id: string): Observable<User> {
    return this.http.get<User>(API_URL + 'users/' + id);
  }

  save(user: User): Observable<any> {
    return this.http.post(API_URL + 'users', user, {observe: 'response'});
  }

  delete(id: string):  Observable<any> {
    return this.http.delete(API_URL + 'users/' + id, {observe: 'response'});
  }

  private _initializeRoles(token: any) {
    const decodedToken = this.jwtHelper.decodeToken(token);
    console.log(decodedToken);
    this.isAdmin = decodedToken.user.role === ADMIN_ROLE;
    this.isEmployee = decodedToken.user.role === EMPLOYEE_ROLE;
    this.accessToken = token;
    this.user = decodedToken.user;
  }

  private _saveToken(token: string) {
    localStorage.setItem(TOKEN_NAME, token);
  }

  login(token: string) {
    this._initializeRoles(token);

    this._saveToken(token);
  }

  logout() {
    this.accessToken = null;
    this.isAdmin = false;
    localStorage.removeItem(TOKEN_NAME);
  }

  isAdminUser(): boolean {
    return this.isAdmin;
  }

  isEmployeeUser(): boolean {
    return this.accessToken && this.isEmployee;
  }

  isUser(): boolean {
    return this.accessToken && !this.isAdmin && !this.isEmployee;
  }

  isLogged(): boolean {
    return this.accessToken && (this.isAdmin || this.isEmployee || this.isUser());
  }
}
