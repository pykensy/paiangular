import * as jsPDF from 'jspdf';
import {Transfer, TRANSFER_STATUSES_NAMES} from '../models/transfer.model';

export function pdfGenerator(data: Transfer[])  {
  const doc = new jsPDF();

  for (let i = 0; i < data.length; i++) {
    doc.text(20, 20, 'Transakcja numer: ' + data[i].id);
    doc.text(20, 30, 'Data transakcji: ' + data[i].transactionDate);
    doc.text(20, 40, 'Kwota transakcji: ' + data[i].amount + ' PLN');
    doc.text(20, 50, 'Typ transakcji: ' + data[i].type.name);
    doc.text(20, 60, 'Status: ' + TRANSFER_STATUSES_NAMES.get(data[i].status));
    doc.text(20, 70, 'Odbiorca');
    doc.text(20, 80, 'Nazwa: ' + data[i].beneficiary.fullName);
    doc.text(20, 90, 'Numer konta: ' + data[i].beneficiary.accountIban);
    if (data[i].beneficiary.address) {
      doc.text(20, 100, 'Adres:' + data[i].beneficiary.address);
    }
    if (data.length !== 1 && i !== data.length - 1) {
      doc.addPage();
    }
  }

  doc.save('Transfer.pdf');
}

