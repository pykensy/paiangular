import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-transfer-summary',
  templateUrl: './transfer-summary.component.html',
  styleUrls: ['./transfer-summary.component.scss']
})
export class TransferSummaryComponent implements OnInit {

  @Input('transferForm')
  transferForm: any;

  constructor() { }

  ngOnInit() {
  }

}
