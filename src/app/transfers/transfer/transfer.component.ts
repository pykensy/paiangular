import {Component} from '@angular/core';
import {TransferType} from '../../shared/models/transfer-type.model';
import {Observable} from 'rxjs/Observable';
import {MyErrorStateMatcher} from '../../shared/validators/error-matcher.validator';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AccountsService} from '../../services/accounts.service';
import {TransferService} from '../../services/transfer.service';
import {UsersService} from '../../services/users.service';
import {Account} from '../../shared/models/account.model';
import {tokenValidator} from '../../shared/validators/token.validator';
import {TransferRequest} from '../../shared/models/transfer-request.model';
import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';

import * as _moment from 'moment';
import {MatSnackBar} from '@angular/material';
import {pdfGenerator} from '../../shared/generators/pdf.generator';
import {Transfer} from '../../shared/models/transfer.model';
import {EXPRESS_ELIXIR, SORBNET} from '../transfer.consts';
// import {default as _rollupMoment} from 'moment';
const moment = _moment;

@Component({
  selector: 'app-transfer',
  templateUrl: './transfer.component.html',
  styleUrls: ['./transfer.component.scss'],
  providers: [
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_LOCALE, useValue: 'pl-PL'},
    {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS},
  ]
})
export class TransferComponent {

  private EXECUTE_TRANSFER_RESULT = 'Przelew został przekazany do realizacji';
  private EXECUTE_TRANSFER_RESULT_ERROR = 'Przelew nie został przekazany do realizacji. Spróbuj ponownie!';

  accounts$: Observable<Account[]>;
  transferTypes$: Observable<TransferType[]>;

  form: FormGroup;
  tokenForm: FormGroup;

  matcher: MyErrorStateMatcher;

  firstStepSubmited = false;
  secondStepSubmited = false;
  waitForTransferExecutionResult = true;
  isDateRequired = true;

  transferRequest: TransferRequest;
  startDate: Date;
  canGeneratePdf = false;


  constructor(public snackBar: MatSnackBar,
              private accountService: AccountsService,
              private usersService: UsersService,
              private transferService: TransferService,
              private fb: FormBuilder) {
    this.accounts$ = accountService.getUserAccounts(this.usersService.user.id);
    this.transferTypes$ = transferService.getTransferTypes();
    this._createForm();
    this.matcher = new MyErrorStateMatcher();
    this.valueChangesListeners();

    const date = new Date();
    this.startDate = new Date(date.getFullYear(), date.getMonth(), date.getDate());
  }

  submitFirstStep() {
    this.firstStepSubmited = true;
    this.transferRequest = this.convertFromForm();
    this._generateToken();
  }

  private _generateToken() {
    // todo if validation error in server show errors and no direct to second form step
    this.transferService.generateToken(this.transferRequest)
      .subscribe(data => {
        console.log(data);
        this.tokenForm.controls.token.setValidators(Validators.compose([Validators.required, tokenValidator(data)]))
      });
  }

  private _createForm() {
    this.form = this.fb.group({
        userAccount: ['', Validators.compose([Validators.required])],
        beneficiaryName: ['', Validators.compose([Validators.required])],
        beneficiaryAccountIban: ['', Validators.compose([Validators.required])],
        beneficiaryAddress: ['', Validators.compose([])],
        type: ['', Validators.compose([Validators.required])],
        amount: ['', Validators.compose([Validators.required, Validators.min(1), Validators.max(99999999999.99)])],
        title: ['', Validators.compose([Validators.required, Validators.maxLength(2000)])],
        date: [this.startDate, Validators.compose([Validators.required])],
        emailConfirm: [''],
        email: ['']
      },
      {
        // validators: Validators.compose([moneyInAccountValidator('userAccount', 'amount')])
      });
    // todo number validation
    this.tokenForm = this.fb.group({
      token: ['']
    });

    this._initMockedFormValues();
  }

  openSnackBar(message: string) {
    this.snackBar.open(message, null, {
      duration: 5000,
    });
  }

  private _initMockedFormValues() {
    this.accounts$.subscribe(data => this.form.controls.userAccount.setValue(data[0]));
    this.form.controls.beneficiaryName.setValue('Jan Kowalski');
    this.form.controls.beneficiaryAccountIban.setValue('705716658025331461613752895300');
    this.form.controls.beneficiaryAddress.setValue('ul. Prosta 21 12-345 Warszawa');
    this.form.controls.amount.setValue(123.45);
    this.form.controls.title.setValue('Przelew za ziemniaki');
    this.form.controls.date.setValue('15.12.2018');
    this.form.controls.emailConfirm.setValue(true);
    this.form.controls.email.setValue('wipekxxx@gmail.com');

  }

  private convertFromForm(): TransferRequest {
    return {
      accountId: this.form.controls.userAccount.value.id,
      beneficiaryName: this.form.controls.beneficiaryName.value,
      beneficiaryAccountIban: this.form.controls.beneficiaryAccountIban.value,
      beneficiaryAddress: this.form.controls.beneficiaryAddress.value,
      transferTypeId: this.form.controls.type.value.id,
      amount: this.form.controls.amount.value,
      title: this.form.controls.title.value,
      date: this.getFormattedDate(),
      emailConfirm: this.form.controls.emailConfirm.value,
      email: this.form.controls.email.value,
      userId: this.usersService.user.id
    };
  }

  private getFormattedDate(): string {
    if (!this.isDateRequired) {
      return '';
    }

    const day = this.form.controls.date.value._i.date;
    const month = this.form.controls.date.value._i.month;

    return (day < 10 ? ('0' + day) : day) + '.' +
      (month < 10 ? ('0' + month) : month) + '.' +
      this.form.controls.date.value._i.year
  }

  private valueChangesListeners(): void {
    this.form.controls.emailConfirm.valueChanges.subscribe(e => {
      this.form.controls.email.setValidators(e ? Validators.email : null);
    });

    this.form.controls.type.valueChanges.subscribe((e: TransferType) => {
      if (e.name.toLowerCase() === SORBNET.toLowerCase() || e.name.toLowerCase() === EXPRESS_ELIXIR.toLowerCase()) {
        this.isDateRequired = false;
        this.form.controls.date.clearValidators();
        this.form.controls.date.setValue(new Date())
      } else {
        this.isDateRequired = true;
        this.form.controls.date.setValidators(Validators.required);
      }
    });
  }

  tryExecuteTransfer() {
    this.secondStepSubmited = true;
    console.log(this.transferRequest);
    this.transferRequest.token = this.tokenForm.controls.token.value;
    console.log(this.transferRequest);
    this.transferService.tryExecuteTransfer(this.transferRequest)
      .subscribe(data => {
        this.waitForTransferExecutionResult = false;
        console.log(data);
        this.openSnackBar(this.EXECUTE_TRANSFER_RESULT);
        this.canGeneratePdf = true;
      },
      error2 =>  {
        this.waitForTransferExecutionResult = false;
        this.openSnackBar(this.EXECUTE_TRANSFER_RESULT_ERROR)
      });
  }

  generatePdf() {
    const pdfData = new Transfer();
    pdfData.id = '';
    pdfData.account = this.form.controls.userAccount.value;
    pdfData.transactionDate = this.form.controls.date.value;
    pdfData.title = this.form.controls.title.value;
    pdfData.amount = this.form.controls.amount.value;
    pdfData.type = this.form.controls.type.value;
    pdfData.beneficiary = {
      accountIban: this.form.controls.beneficiaryAccountIban.value,
      fullName: this.form.controls.beneficiaryName.value,
      address: this.form.controls.beneficiaryAddress.value
    };
    pdfGenerator([pdfData]);
  }
}
