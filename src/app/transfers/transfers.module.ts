import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TransferComponent} from './transfer/transfer.component';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SharedModule} from '../shared/shared.module';
import {TransfersRoutingModule} from './transfers.routing';
import {TransferService} from '../services/transfer.service';
import {TransferSummaryComponent} from './transfer/transfer-summary/transfer-summary.component';

@NgModule({
  imports: [
    BrowserModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    TransfersRoutingModule
  ],
  declarations: [TransferComponent, TransferSummaryComponent],
  providers: [TransferService],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TransfersModule {
}
