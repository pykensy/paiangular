import {RouterModule, Routes} from '@angular/router';
import {AuthGuard} from '../shared/guards/auth-guard.service';
import {NgModule} from '@angular/core';
import {TransferComponent} from './transfer/transfer.component';

const routes: Routes = [
  {
    path: 'transfer',
    component: TransferComponent,
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class TransfersRoutingModule {
}
